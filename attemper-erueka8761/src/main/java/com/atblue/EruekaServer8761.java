package com.atblue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * description: EruekaServer8761 <br>
 * date: 2022/8/29 21:35 <br>
 * author: yan <br>
 * version: 1.0 <br>
 */
@SpringBootApplication
@EnableEurekaServer
public class EruekaServer8761 {
    public static void main(String[] args) {
        SpringApplication.run(EruekaServer8761.class, args);
    }
}
